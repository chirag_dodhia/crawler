Rails.application.routes.draw do
  get 'posts' => "crawl#crawl", :as => "crawl"
  get 'posts/archive' => "crawl#crawl", :as => "crawl_preview"
  root "application#welcome", :as => "home", via: :all
  post 'blog(/:title)' => "crawl#blog", :as => "blog"
end
