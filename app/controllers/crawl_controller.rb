class CrawlController < ApplicationController
  def crawl
    @crawl_data = Rails.cache.fetch("#{params['tag']}/#{params['format']}", expires_in: 12.hours) do
      Crawl.crawl_data(params['tag'], params[:format])
    end
  end

  def blog
    time_start = Time.now
    @blog = Rails.cache.fetch("#{params['link']}", expires_in: 12.hours) do
      Crawl.blog_data(params['link'])
    end
    time_end = Time.now
    @blog[:response_time] = time_end - time_start
  end
end
