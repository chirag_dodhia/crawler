class Crawl < ApplicationRecord
  def self.crawl_data(tag, next_posts = nil)
    url = "https://medium.com/tag/#{tag}/#{next_posts}"
    unparsed_data = HTTParty.get(url)
    parsed_data = Nokogiri::HTML(unparsed_data)
    posts = parsed_data.css('.streamItem')
    blogs = []
    posts.each do |post|
      author = post.css('.u-accentColor--textDarken').text()
      title = post.css('h3').text()
      details = post.css('time').text() + " " + post.css('span.readingTime').first['title']
      link_title = post.css('a').last['href'].split('.').last
      link = post.css('a').last['href']
      link_title = link_title.index('/') ? link_title[link_title.index('/')+1..link_title.length] : link_title
      img = post.css("img").last['src']
      blogs << {author: author, title: title, details: details, link_title: link_title, link: link, img: img}
    end
    blogs
  end

  def self.blog_data(link)
    data = HTTParty.get(link)
    post = Nokogiri::HTML(data)
    tags_data = (tags_data = post.at_xpath("//script[@type='application/ld+json']")).present? && JSON.parse(tags_data.children.text())['keywords']
    tags = (tags_data.present? && tags_data.map{|tag| tag.include?('Tag') && tag[4..tag.length]}.select{|tag| tag!=false})
    blog_data = post.css('article').css('div').first.css('section').first.inner_html
    {blog_data: blog_data, tags: tags}
  end
end
